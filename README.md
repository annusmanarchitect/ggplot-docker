# ggplot-docker


## Getting started

This template repository has basic getting started files for using **ggplot** (which is in **R**) in Python using `rpy2`. This is provided as a docker container so that the only required setup is docker (please follow the [following document](https://docs.docker.com/engine/install/ubuntu/) followed by [this](https://docs.docker.com/engine/install/linux-postinstall/) to properly setup docker).

## Starting the ggplot docker

- Start `rpy2` docker in the project root directory
```sh
cd ggplot-docker
# the following should only be run once
chmod +x run-jupyter.sh
# start the rpy2 docker container
make
```
Then copy the link shown in the output (something like `http://127.0.0.1:8888/?token=blablablablablabla` and open in your browser). You have been given a number of example plots in this jupyter notebook along with some datasets in [data](data/) directory which you should be able to plot.

## Cleanup
```sh
# remove any unwanted files before pushing to git
make clean
```

